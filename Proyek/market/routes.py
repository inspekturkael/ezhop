from market import app
from flask import render_template, redirect, url_for, flash, request
from market.models import Item, User #ambil models (Item) dan (User)
from market.forms import PurchaseItemForm, RegisterForm, LoginForm, SellItemForm #ambil class RegisterForm dan LoginForm
from flask_login import login_user, logout_user, login_required, current_user
from market import db

@app.route('/')
@app.route('/home')
def home_page():
    return render_template('home.html')

@app.route('/market', methods=['GET', 'POST'])
@login_required
def market_page():
    purchase_form = PurchaseItemForm()
    selling_form = SellItemForm()
    if request.method == "POST":
        # Purchase item
        purchased_item = request.form.get('purchased_item')
        p_item_object = Item.query.filter_by(name=purchased_item).first()
        
        if p_item_object:

            if current_user.have_budget(p_item_object.price):
                p_item_object.buy(current_user)
                flash(f"Thanks for buying! You purchased {p_item_object.name}!", category="success")
            
            else:
                flash(f"Unfortunately, you don't have enough budget to purchase {p_item_object.name}", category="danger")
        
        # Sell item
        sold_item = request.form.get('sold_item')
        s_item_object = Item.query.filter_by(name=sold_item).first()
        if s_item_object:
            if current_user.can_sell(s_item_object):
                s_item_object.sell(current_user)
                flash(f"Congrats! You sold {s_item_object.name}!", category="success")
            else:
                flash(f"Sorry, something when wrong with selling {s_item_object.name}!", category="danger")

        return redirect(url_for('market_page'))

    if request.method == "GET":
        items = Item.query.filter_by(owner=None)
        owned_items = Item.query.filter_by(owner=current_user.id)
        return render_template('market.html', items=items, purchase_form=purchase_form, owned_items=owned_items, selling_form=selling_form)
    

@app.route('/register', methods=['GET', 'POST'])
def register_page():
    form = RegisterForm()

    # VALIDATE when the user is click the submit button
    # Dia ngecek 2 kondisi berarti, 1. validasinya terlebih dahulu, 2. Kondisi ketika sudah ditekan (rising edge)
    if form.validate_on_submit():
        user_to_create = User(username=form.username.data,
                              email_address=form.email_address.data,
                              password=form.password.data)
        db.session.add(user_to_create)
        db.session.commit()
        flash(f'Success Create New Account!', category='success')
        return redirect(url_for('market_page'))

    if form.errors != {}: # if there's not error
        for error in form.errors.values():
            flash(f'Creating User was Rejected: {error}', category='danger')


    return render_template('register.html', form=form)

@app.route('/login', methods=['GET', 'POST'])
def login_page():
    form = LoginForm()

    if form.validate_on_submit():
        attempted_user = User.query.filter_by(username=form.username.data).first()
        if attempted_user and attempted_user.check_password(this_password=form.password.data): 
            login_user(attempted_user)
            flash(f'Success! Welcome to EZhop, {attempted_user.username}!', category='success')
            return redirect(url_for('market_page'))

        else:
            flash('Sorry, your username and password are not match. Please try again', category='danger')

    return render_template('login.html', form=form)


@app.route('/logout')
def logout_page():
    logout_user()
    flash("See You! Thanks for Coming!", category='info')
    return redirect(url_for('home_page'))