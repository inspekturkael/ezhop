from re import search
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import DataRequired, Email, Length, EqualTo, ValidationError
from market.models import User

class RegisterForm(FlaskForm):

    def validate_username(self, this_username):
        # search for the object, if there's already exists, will thrown an error
        user = User.query.filter_by(username=this_username.data).first()
        if user:
        # if search(User.query.all(), this_username):
            raise ValidationError('Username Already Exists!')

    def validate_email_address(self, this_email_address):
        email_address = User.query.filter_by(email_address=this_email_address.data).first()
        if email_address:
        # if search(User.query.all(), this_email_address):
            raise ValidationError('Email Already Exists!')     
            
    # DataRequired is asking the user to fill the field
    username = StringField(label='Username: ', validators=[Length(min=2, max=30), DataRequired()]) 
    email_address = StringField(label='Email: ', validators=[Email(), DataRequired()])
    password = PasswordField(label='Password: ', validators=[Length(min=8), DataRequired()])
    password_confirm = PasswordField(label='Confirm Password: ', validators=[EqualTo('password'), DataRequired()])
    submit = SubmitField(label='Create Account')

class LoginForm(FlaskForm):
    username = StringField(label='Username: ', validators=[DataRequired()]) 
    password = PasswordField(label='Password: ', validators=[DataRequired()])
    submit = SubmitField(label='Enter EZhop')

class PurchaseItemForm(FlaskForm):
    submit = SubmitField(label='Yes, Purchase the Item!')

class SellItemForm(FlaskForm):
    submit = SubmitField(label='Yes, Sell the Item!')